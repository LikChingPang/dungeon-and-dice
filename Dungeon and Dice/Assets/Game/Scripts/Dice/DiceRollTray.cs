using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Dice Roll Tray Identifier, should only have 1 at the same time
/// </summary>
public class DiceRollTray : Singleton<DiceRollTray>
{
    // Method to return all roll results
    public int GetRollResults()
    {
        // Initlaize an int to store the roll results
        int rollResults = 0;

        // Calculate the total amount of roll results
        for (int i = 0; i < transform.childCount; i++)
        {
            rollResults += transform.GetChild(i).GetComponent<Dice>().rollResult;
        }

        return rollResults;
    }

    // Method to destroy all children gameObjects (dice)
    public void RemoveDice()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
}
