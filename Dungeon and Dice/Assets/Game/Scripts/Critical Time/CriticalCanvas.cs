using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Critical canvas identifier and hold references for its child
/// </summary>
[RequireComponent(typeof(Canvas))]
public class CriticalCanvas : MonoBehaviour
{
    [Header("References")]
    public RectTransform backgroundPanel;
    public TMP_Text criticalTimeQuestionText;
    public Transform answerButtonsParent;
    public Slider criticalTimeTimer;

    [Header("Events")]
    [Tooltip("Raise when the user click on this button")]
    public UnityEvent<int> FinishCritical;

    void Update()
    {
        // Decrease the timer by real time passed
        criticalTimeTimer.value -= Time.deltaTime;

        // If the time is up
        if (criticalTimeTimer.value <= 0.0f)
        {
            try
            {
                // End turn
                // Invoke an event to tell the ciritical manager that the user picked an answer and pass over the final damage / healing done of the skill
                FinishCritical.Invoke(CriticalManager.Instance.currentSkillAmount);
            }
            catch
            {
                Debug.LogError($"Critical Manager does not exist!");
            }
        }
    }
}
