using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manager to handle the critical time behaviour
/// </summary>
public class CriticalManager : Singleton<CriticalManager>
{
    [Header("Reference")]
    [Tooltip("Critical answer button game object prefeb")]
    [SerializeField] GameObject criticalAnswerButtonGameObjectPrefeb = null;
    [Tooltip("Critical Canvas game object prefeb")]
    [SerializeField] CriticalCanvas criticalCanvasGameObjectPrefeb = null;
    [Tooltip("Critical Canvas in the game")]
    CriticalCanvas criticalCanvas = null;
    [Tooltip("The critical critical answer buttons parent transform in the game")]
    Transform criticalAnswerButtonsParent = null;

    [Header("Data")]
    [Tooltip("Time limit for answering the critical time question")]
    [SerializeField] float criticalTimeLimit = 0.0f;
    [Tooltip("How many answer button should there be for the question?")]
    [SerializeField] int answerButtonNumber = 1;
    [Tooltip("What question type are we asking?")]
    [SerializeField] QuestionType questionType = QuestionType.rollResult;
    [Tooltip("The current skill that has a chance to land a critical hit")]
    [ReadOnly] public Skill currentSkill = null;
    [Tooltip("The current source of the skill")]
    [ReadOnly] public Character currentSource = null;
    [Tooltip("The current target of the skill")]
    [ReadOnly] public Character currentTarget = null;
    [Tooltip("The current damage / healing amount of the skill")]
    [ReadOnly] public int currentSkillAmount = 0;

    private enum QuestionType
    { 
        rollResult,
        currenttotalDamage
    }

    void OnValidate()
    {
        // If the designer input a number less than 1 for the number of answer button
        if (answerButtonNumber < 1)
        {
            // Set it back to 1
            answerButtonNumber = 1;
        }

        // If the designer input a negative number time limit
        if (criticalTimeLimit < 0)
        {
            // Set it back to 0
            criticalTimeLimit = 0;
        }
    }

    void Start()
    {
        // If there is a critical canvas
        if (criticalCanvas == null)
        {
            // Instantiate a critical canvas for the game and initialize it
            criticalCanvas = Instantiate(criticalCanvasGameObjectPrefeb);
        }

        // Hide the critical canvas
        criticalCanvas.gameObject.SetActive(false);

        // Initialize
        criticalAnswerButtonsParent = criticalCanvas.answerButtonsParent;
        criticalCanvas.criticalTimeTimer.maxValue = criticalTimeLimit;
    }

    // Method to update the current skill that has a chance to land a critical hit
    public void UpdateCurrentSkillInfo(Skill skill, Character source, Character target, int amount)
    {
        // Throw a debug message
        Debug.Log("Updating crit info...");

        currentSkill = skill;
        currentSource = source;
        currentTarget = target;
        currentSkillAmount = amount;
    }

    // Method to popup the critical canvas
    public void PopupCriticalCanvas()
    {
        try
        {
            criticalCanvas.gameObject.SetActive(true);
            criticalCanvas.criticalTimeTimer.value = criticalTimeLimit;
        }
        catch
        {
            // Throw a debug message
            Debug.LogError("There is no critical canvas in the game scene!");
        }
    }

    // Method to reset and hide the critical canvas
    public void ResetAndHideCriticalCanvas()
    {
        // Reset the correct answer button
        for (int i = 0; i < criticalAnswerButtonsParent.childCount; i++)
        {
            try
            {
                criticalAnswerButtonsParent.GetChild(i).GetComponent<CriticalTimeAnswerButton>().correctAnswer = false;
            }
            catch
            {
                // Throw a debug message
                Debug.LogError($"{criticalAnswerButtonsParent.GetChild(i).name} does not have a CriticalTimeAnswerButton conponent!");
            }
        }

        // Hide the critical canvas
        criticalCanvas.gameObject.SetActive(false);
    }

    public void SetAnswerButtons()
    {
#region Safety Measure
        // Safety measure to ensure there are enough answer buttons
        try
        {
            // If there are not enough answer buttons under criticalAnswerButtonsParent
            if (criticalAnswerButtonsParent.childCount < answerButtonNumber)
            {
                try
                {
                    // Calculate the difference between answerButtonNumber and criticalAnswerButtonsParent's child count
                    int difference = answerButtonNumber - criticalAnswerButtonsParent.childCount;

                    // Instantiate the missing number of criticalAnswerButtonGameObjectPrefeb under criticalAnswerButtonsParent
                    for (int i = 0; i < difference; i++)
                    {
                        Instantiate(criticalAnswerButtonGameObjectPrefeb, criticalAnswerButtonsParent);
                    }
                }
                catch
                {
                    // Throw a debug message
                    Debug.LogError("There is no criticalAnswerButtonGameObjectPrefeb!");
                }
            }
            // If there are too many answer buttons under criticalAnswerButtonsParent for some odd reason
            else if (criticalAnswerButtonsParent.childCount > answerButtonNumber)
            {
                // Calculate the difference between answerButtonNumber and criticalAnswerButtonsParent's child count
                int difference = criticalAnswerButtonsParent.childCount - answerButtonNumber;

                // Destroy the excess number of criticalAnswerButtonGameObjectPrefeb under criticalAnswerButtonsParent
                // Destroy call last so bugs may occur
                for (int i = 0; i < difference; i++)
                {
                    Destroy(criticalAnswerButtonsParent.GetChild(i).gameObject);
                }
            }
        }
        catch
        {
            // Throw a debug message
            Debug.LogError("There is no criticalAnswerButtonsParent!");
        }
#endregion

        // Determine the correct answer button for the question
        int correctAnswerButtonIndex = Random.Range(0, criticalAnswerButtonsParent.childCount);

        try
        {
            // Set that button to be the correct answer button
            criticalAnswerButtonsParent.GetChild(correctAnswerButtonIndex).GetComponent<CriticalTimeAnswerButton>().correctAnswer = true;
        }
        catch
        {
            // Throw a debug message
            Debug.LogError($"{criticalAnswerButtonsParent.GetChild(correctAnswerButtonIndex).name} does not have the CriticalTimeAnswerButton component!");
        }

        // Initialize a temp int to store the correct answer number of the question
        int correctAnswer = 0;

        // If we are asking for sum of roll result
        if (questionType == QuestionType.rollResult)
        {
            // Get the roll result
            correctAnswer = DiceRollTray.Instance.GetRollResults();
        }

        // Set the answer button's text
        for (int i = 0; i < criticalAnswerButtonsParent.childCount; i++)
        {
            try
            {
                // Cache the answer button
                CriticalTimeAnswerButton currentAnswerButton = criticalAnswerButtonsParent.GetChild(i).GetComponent<CriticalTimeAnswerButton>();

                // If that button is the correct answer button
                if (currentAnswerButton.correctAnswer == true)
                {
                    // Set the correct answer text
                    currentAnswerButton.buttonText.text = correctAnswer.ToString();
                }
                // If that button is the wrong answer button
                else
                {
                    // Set button text depending on the index of the button
                    // If this button's index is before the correct answer button index
                    if (i < correctAnswerButtonIndex)
                    {
                        // Make the text number smaller than the correct answer button by minusing the difference of the index
                        currentAnswerButton.buttonText.text = (correctAnswer - (correctAnswerButtonIndex - i)).ToString();
                    }
                    // If this button's index is after the correct answer button index
                    else if (i > correctAnswerButtonIndex)
                    {
                        // Make the text number larger than the correct answer button by adding the difference of the index
                        currentAnswerButton.buttonText.text = (correctAnswer + (i - correctAnswerButtonIndex)).ToString();
                    }
                }
            }
            catch
            {
                // Throw a debug message
                Debug.LogError($"{criticalAnswerButtonsParent.GetChild(i).name} does not have the CriticalTimeAnswerButton component!");
            }
        }
    }

    // Method to execute a skill during critical time
    public void ExecuteSkill(int amount)
    {
        // Execute the skill
        currentSkill.ExecuteSkill(currentSource, currentTarget, amount);
    }
}
