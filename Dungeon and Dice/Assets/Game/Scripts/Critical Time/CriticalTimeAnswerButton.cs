using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class CriticalTimeAnswerButton : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Text of this button")]
    [ReadOnly] public TMP_Text buttonText;

    [Header("Data")]
    [Tooltip("Is this answer button responsible for the correct answer of the question?")]
    [HideInInspector] public bool correctAnswer = false;

    [Header("Events")]
    [Tooltip("Raise when the user click on this button")]
    public UnityEvent<int> FinishCritical;

    void Awake()
    {
        // Initialize
        correctAnswer = false;
    }

    // Method called when user click on this answer button
    // Check if the user clicked the correct button and execute the skill
    public void CheckCorrect()
    {
        // Initialize a temp int to calculate the final damage / healing amount
        int finalAmount = CriticalManager.Instance.currentSkillAmount;

        // If I am the correct answer
        if (correctAnswer == true)
        {
            // Throw a debug message
            Debug.Log("Correct answer!");

            // Multiply the damage / healing value by the critical multiplier
            finalAmount = (int)(finalAmount * GameManager.Instance.criticalMultiplier);
        }
        // If I am not the correct answer
        else
        {
            // Throw a debug message
            Debug.Log("Wrong answer!");
        }

        // Invoke an event to tell the ciritical manager that the user picked an answer and pass over the final damage / healing done of the skill
        FinishCritical.Invoke(finalAmount);
    }
}
