using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Transition Canvas identifier, also responsible for the canvas' behaviour
/// </summary>
public class TransitionCanvas : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Blackscreen's animator")]
    [SerializeField] Animator backscreen;

    public void ToBlackScreen()
    {
        backscreen.SetTrigger("Blackscreen");
    }
}
