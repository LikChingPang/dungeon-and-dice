using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Damage popup text identifier
/// Notify the game manager that a turn is over on destroy
/// </summary>
public class DamagePopupText : MonoBehaviour
{
    [Header("Events")]
    [Tooltip("Raise right before this popup text destroy itself")]
    public UnityEvent EndTurn;

    // Method to destroy itself
    private void DestroySelf()
    {
        // Notify the game manager that a turn is over and the game manager should determine if the turn is actually over
        // aka if a lot of damage popup text, it is game manager's responsibility to determine if a turn should end
        EndTurn.Invoke();

        // Destroy itself
        Destroy(gameObject);
    }
}
