using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// The base clase for all enemies in the game
/// </summary>
[RequireComponent(typeof(BoxCollider2D))]
public class Enemy : Character
{
    [Header("Enemy")]
    [Tooltip("Current attack of the enemy")]
    [SerializeField] [ReadOnly] int attack = 0;
    [Tooltip("How long should they wait before they take their turn")]
    [SerializeField] float timeBeforeTakingTurn = 0.0f;
    [Tooltip("Stats scriptable object to determine the enemy's starting stats")]
    public EnemyStats enemyStats;

    [Tooltip("Raise this when a character's attack needs to be updated")]
    public UnityEvent<Character, int> UpdateCharacterAttack;

    void Awake()
    {
        // Initialize
        attack = enemyStats.attack;
        maximumHealth = enemyStats.maximumHealth;
        currentHealth = maximumHealth;
        shield = 0;

        // Initialize UI
        try
        {
            healthBar.maxValue = maximumHealth;
            healthBar.value = currentHealth;
            healthText.text = $"{currentHealth} / {maximumHealth}";
        }
        catch
        {
            // Throw a debug message
            Debug.Log("There is no health UI!");
        }

        // Add this enemy into Game Manager's enemyList
        GameManager.Instance.AddEnemy(this);
    }

    public override void TakeDamage(Character source, int damage)
    {
        // If the enemy is dead already
        if (currentHealth <= 0)
        {
            // Throw a debug message
            Debug.Log($"{gameObject.name} is dead already!");

            // Ignore the take damage request
            return;
        }

        // Go though the base code from character
        base.TakeDamage(source, damage);

        // See if the enemy is dead
        if (currentHealth <= 0)
        {
            // Die
            Death();
        }
    }

    // Method called when the enemy dies
    protected override void Death()
    {
        // Throw a debug message
        Debug.Log("Enemy is dead!");

        // Execute the base character's code
        base.Death();

        // Tell the game manager that I am dead
        GameManager.Instance.RemoveEnemy(this);
    }

    // Method to change current attack (can be increase or decrease)
    public void ChangeCurrentAttack(int amount)
    {
        // Throw a debug message
        Debug.Log($"{gameObject.name} is changing {amount} amount of attack!");

        // Change the attack by the amount
        attack += amount;

        // Tell the others that my attack changed
        UpdateCharacterAttack.Invoke(this, attack);

        // Throw a debug message
        Debug.Log($"{gameObject.name} has {attack} attack now!");
    }

    // Method called when the character takes their turn
    protected override void TakeTurn()
    {
        // Take their turn
        StartCoroutine(TakingTurn());
    }

    protected virtual IEnumerator TakingTurn()
    {
        // Wait for some time to simulate a calculation
        yield return new WaitForSeconds(timeBeforeTakingTurn);

        // Attack a random player
        Attack(GameManager.Instance.playerList[Random.Range(0, GameManager.Instance.playerList.Count)], attack);
    }
}
