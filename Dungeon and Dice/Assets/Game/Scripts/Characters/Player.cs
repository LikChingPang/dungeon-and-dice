using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// The base clase for all players in the game
/// </summary>
public class Player : Character
{
    [Header("Player")]
    [Tooltip("Stats scriptable object to determine the player's starting stats")]
    public PlayerStats playerStats;

    [Header("Events")]
    [Tooltip("Raise when the character initialize")]
    public UnityEvent<Player> AddPlayer;
    [Tooltip("Raise when the character should take their turn")]
    public UnityEvent<SkillData[]> SetupSkillButtons;

    void Awake()
    {
        // Initialize
        maximumHealth = playerStats.maximumHealth;
        currentHealth = maximumHealth;
        shield = 0;

        // Initialize UI
        try
        {
            healthBar.maxValue = maximumHealth;
            healthBar.value = currentHealth;
            healthText.text = $"{currentHealth} / {maximumHealth}";
        }
        catch
        {
            // Throw a debug message
            Debug.Log("There is no health UI!");
        }

        // Add this player into Game Manager's playerList
        AddPlayer.Invoke(this);
    }

    public override void TakeDamage(Character source, int damage)
    {
        // If the player is dead already
        if (currentHealth <= 0)
        {
            // Throw a debug message
            Debug.Log($"{gameObject.name} is dead already!");

            // Ignore the take damage request
            return;
        }

        // Go though the base code from character
        base.TakeDamage(source, damage);

        // See if the player is dead
        if (currentHealth <= 0)
        {
            // Die
            Death();
        }
    }

    // Method called when the player dies
    protected override void Death()
    {
        // Throw a debug message
        Debug.Log("Player is dead!");

        // Execute the base character's code
        base.Death();

        // Remove this player from the player list in the Game Manager
        GameManager.Instance.RemovePlayer(this);
    }

    // Method called when the character takes their turn
    protected override void TakeTurn()
    {
        // Invoke something
        SetupSkillButtons.Invoke(playerStats.selectedSkills);
    }
}
