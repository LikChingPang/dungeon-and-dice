using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Skill), typeof(CanvasGroup))]
public class SkillButton : MonoBehaviour
{
    [Header("Reference")]
    [Tooltip("Canvas group component of this skill button")]
    CanvasGroup myCanvasGroup;

    [Header("Events")]
    [Tooltip("Raise when a skill button is clicked")]
    public UnityEvent SkillSelected;
    [Tooltip("Raise when there is no current target")]
    public UnityEvent<Transform> TryChangeTarget;
    [Tooltip("Raise when there is a chance the user lands a critical hit")]
    public UnityEvent<Skill, Character, Character, int> CriticalTime;

    void Awake()
    {
        // Initialize
        myCanvasGroup = GetComponent<CanvasGroup>();
    }

    // Method called when a skill button is selected
    public void HideAllSkillButtons()
    {
        SkillSelected.Invoke();
    }

    // Method to execute a skill
    public void ExecuteSkill()
    {
        // Get a reference to the parent's skill component
        Skill skill = transform.GetComponent<Skill>();

        try
        {
            // If the current encounter is a battle
            if (GameManager.Instance.currentEncounter.GetType() == typeof(Battle))
            {
                // If the skill is a damaging skill
                if (skill.skillData.GetType() == typeof(DamageSkillData))
                {
                    // If there is no enemy target yet
                    if (GameManager.Instance.currentEnemyTarget == null)
                    {
                        try
                        {
                            // Try to set the target at the first enemy in the enemy list in the game manager
                            TryChangeTarget.Invoke(GameManager.Instance.enemyList[0].transform);
                        }
                        catch
                        {
                            // Throw a debug message
                            Debug.Log("There is no enemy left!");
                        }
                    }
                }
                // If the skill is a healing or shielding skill
                else if (skill.skillData.GetType() == typeof(HealingSkillData) || skill.skillData.GetType() == typeof(ShieldingSkillData))
                {
                    // Cast the skill as a support skill
                    SupportSkillData supportSkillData = (SupportSkillData)skill.skillData;

                    // If the skill can only target the caster
                    // If the healing skill can only target the caster
                    if (supportSkillData.targetSelfOnly == true)
                    {
                        // Try to set the target at the caster in the game manager
                        TryChangeTarget.Invoke(GameManager.Instance.currentPlayer.transform);
                    }

                    // If there is no player target yet
                    if (GameManager.Instance.currentPlayerTarget == null)
                    {
                        try
                        {
                            // Try to set the target at the first player in the player list in the game manager
                            TryChangeTarget.Invoke(GameManager.Instance.playerList[0].transform);
                        }
                        catch
                        {
                            // Throw a debug message
                            Debug.Log("There is no player left!");
                        }
                    }
                }

                // Roll the dice and execute the skill
                StartCoroutine(RollDiceAndExecuteSkill(skill));
            }
        }
        catch
        {
            // Throw a debug message
            Debug.Log("Unknown encounter type!");
        }
    }
    IEnumerator RollDiceAndExecuteSkill(Skill skill)
    {
        // Intialize a temp transform prefeb to store the die object prefeb
        Dice die = null;

        // Determine the die object prefeb needed
        for (int i = 0; i < DiceManager.Instance.dieMatching.Length; i++)
        {
            // If the die size in the skill is the same as the one in dice manager
            if (skill.dieSize == DiceManager.Instance.dieMatching[i].dieSize)
            {
                die = DiceManager.Instance.dieMatching[i].diePrefeb;
            }
        }

        // Instantiate a die object for every die needed and roll them
        for (int i = 0; i < skill.numberOfDie; i++)
        {
            // Instantiate the die
            Dice dice = Instantiate(die, DiceRollTray.Instance.transform);

            // Roll the die
            dice.RollDie();
        }

        // Wait for the die to finish the roll
        yield return new WaitForSeconds(die.rollingTime);

        // Execute the skill

        // Initialize a temp int to calculate the total damage and get the initial roll result
        int result = DiceRollTray.Instance.GetRollResults();

        // Factor in the modifier
        Stats.StatModifier modifier = skill.skillData.modifier;

        // Find the modifer
        for (int i = 0; i < GameManager.Instance.currentPlayer.playerStats.characterStat.Length; i++)
        {
            if (GameManager.Instance.currentPlayer.playerStats.characterStat[i].statModifier == modifier)
            {
                // Add the modifier
                result += GameManager.Instance.currentPlayer.playerStats.characterStat[i].amount;

                // Stop the search
                break;
            }
        }

        // If the skill is a damaging skill
        if (skill.skillData.GetType() == typeof(DamageSkillData))
        {
            // Factor in type effectiveness
            float floatResult = result * GameManager.Instance.effectivenessTable.effectiveness[(int)skill.skillData.skillType, (int)GameManager.Instance.currentEnemyTarget.enemyStats.type];

            // Cast the result to int
            result = (int)floatResult;

            // Execute the skill on the targeted player, no matter it is an AOE or not
            DetermineCritical(skill, GameManager.Instance.currentPlayer, GameManager.Instance.currentEnemyTarget, result);
        }
        // If the skill is a healing or shielding skill
        else if (skill.skillData.GetType() == typeof(HealingSkillData) || skill.skillData.GetType() == typeof(ShieldingSkillData))
        {
            // Execute the skill on the targeted player, no matter it is an AOE or not
            DetermineCritical(skill, GameManager.Instance.currentPlayer, GameManager.Instance.currentPlayerTarget, result);
        }
    }

    // Method to determine if a skill lands a critical hit, then execute the skill if there is no critical
    private void DetermineCritical(Skill skill, Character source, Character target, int amount)
    {
        // Determine if there is a critical chance for the user
        float critcalResult = Random.Range(0.0f, 100.0f);

        // If the player gets a critial hit chance
        if (GameManager.Instance.percentageOfCritical >= critcalResult)
        {
            // Throw a debug message
            Debug.Log("Critical time!");

            // Raise the event of Critical Time (Pop up the critial UI and pass over the variables)
            CriticalTime.Invoke(skill, source, target, amount);
        }
        // If the skill does not land a critical
        else
        {
            // Execute the skill on the target, no matter it is an AOE or not
            skill.ExecuteSkill(source, target, amount);
        }
    }

    // Method to show the skill button
    public void ShowSkillButton()
    {
        myCanvasGroup.alpha = 1;
        myCanvasGroup.interactable = true;
    }

    // Method to hide the skill button
    public void HideSkillButton()
    {
        myCanvasGroup.alpha = 0;
        myCanvasGroup.interactable = false;
    }
}
