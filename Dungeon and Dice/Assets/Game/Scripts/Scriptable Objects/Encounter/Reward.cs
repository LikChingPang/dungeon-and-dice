using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object to store data for rewards of an encounter
/// </summary>
[CreateAssetMenu(fileName = "Encounter Reward", menuName = "Encounter/Reward")]
public class Reward : ScriptableObject
{
    [Header("Data")]
    [Tooltip("Dice reward of that encounter")]
    public DungeonInventory.DiceNumber[] diceReward;
}
