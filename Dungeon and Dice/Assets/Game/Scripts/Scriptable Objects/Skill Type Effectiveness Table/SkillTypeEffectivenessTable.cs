using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object that stores the table of skill types' effectiveness against each other
/// Can only change within this script
/// Tools to edit this table should be done later
/// </summary>

[CreateAssetMenu(fileName = "Effectiveness Table", menuName = "Skill/Effectiveness Table")]
public class SkillTypeEffectivenessTable : ScriptableObject
{
    public float[,] effectiveness = new float[4, 4];

    void Awake()
    {
        // Initialize effectiveness
        // Melee to Melee
        effectiveness[0, 0] = 1;
        // Melee to Ranged
        effectiveness[0, 1] = 0.5f;
        // Melee to Magic
        effectiveness[0, 2] = 2;
        // Melee to Support
        effectiveness[0, 3] = 1;
        // Ranged to Melee
        effectiveness[1, 0] = 2;
        // Ranged to Ranged
        effectiveness[1, 1] = 1;
        // Ranged to Magic
        effectiveness[1, 2] = 0.5f;
        // Ranged to Support
        effectiveness[1, 3] = 1;
        // Magic to Melee
        effectiveness[2, 0] = 0.5f;
        // Magic to Ranged
        effectiveness[2, 1] = 2;
        // Magic to Magic
        effectiveness[2, 2] = 1;
        // Magic to Support
        effectiveness[2, 3] = 1;
        // Support to Melee
        effectiveness[3, 0] = 1;
        // Support to Ranged
        effectiveness[3, 1] = 1;
        // Support to Magic
        effectiveness[3, 2] = 1;
        // Support to Support
        effectiveness[3, 3] = 1;
    }

    private void OnValidate()
    {
        // Initialize effectiveness
        // Melee to Melee
        effectiveness[0, 0] = 1;
        // Melee to Ranged
        effectiveness[0, 1] = 0.5f;
        // Melee to Magic
        effectiveness[0, 2] = 2;
        // Melee to Support
        effectiveness[0, 3] = 1;
        // Ranged to Melee
        effectiveness[1, 0] = 2;
        // Ranged to Ranged
        effectiveness[1, 1] = 1;
        // Ranged to Magic
        effectiveness[1, 2] = 0.5f;
        // Ranged to Support
        effectiveness[1, 3] = 1;
        // Magic to Melee
        effectiveness[2, 0] = 0.5f;
        // Magic to Ranged
        effectiveness[2, 1] = 2;
        // Magic to Magic
        effectiveness[2, 2] = 1;
        // Magic to Support
        effectiveness[2, 3] = 1;
        // Support to Melee
        effectiveness[3, 0] = 1;
        // Support to Ranged
        effectiveness[3, 1] = 1;
        // Support to Magic
        effectiveness[3, 2] = 1;
        // Support to Support
        effectiveness[3, 3] = 1;
    }
}
