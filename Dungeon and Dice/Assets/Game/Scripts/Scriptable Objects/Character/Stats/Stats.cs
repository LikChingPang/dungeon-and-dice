using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract scriptable object that store all the stats on start for a character
/// </summary>
public abstract class Stats : ScriptableObject
{
    [Header("Stats")]
    [Tooltip("Maximum health of the character")]
    public int maximumHealth = 0;
    [Tooltip("Type of the character, aka what element does the character belong to")]
    public SkillData.SkillType type;

    public enum StatModifier
    {
        strength,
        dexterity,
        intelligence,
        wisdom
    }
}
