using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object that store all the stats on start for a player character
/// </summary>
[CreateAssetMenu(fileName = "Player Stats", menuName = "Character/Player Stats")]
public class PlayerStats : Stats
{
    [Header("Player Stats")]
    [Tooltip("The class / job for the player")]
    public Job job;
    [Tooltip("Current skill set selected by the player")]
    public SkillData[] selectedSkills;
    [Tooltip("Stat modifiers of the player character")]
    public CharacterStat[] characterStat = new CharacterStat[4];

    [System.Serializable]
    public struct CharacterStat
    {
        [ReadOnly] public StatModifier statModifier;
        public int amount;
    }

    void Awake()
    {
        // Initialize
        characterStat[0].statModifier = StatModifier.strength;
        characterStat[1].statModifier = StatModifier.dexterity;
        characterStat[2].statModifier = StatModifier.intelligence;
        characterStat[3].statModifier = StatModifier.wisdom;
    }

    void OnValidate()
    {
        // Do not allow designers to add new modifiers unless the programmers allow them to
        if (characterStat.Length != System.Enum.GetValues(typeof(StatModifier)).Length)
        {
            // Cache the enum length
            int StatModifierLength = System.Enum.GetValues(typeof(StatModifier)).Length;

            // Initialize a temp CharacterStat array
            CharacterStat[] characterStats = new CharacterStat[StatModifierLength];

            // Initialize all the data for statModifier
            characterStats[0].statModifier = StatModifier.strength;
            characterStats[1].statModifier = StatModifier.dexterity;
            characterStats[2].statModifier = StatModifier.intelligence;
            characterStats[3].statModifier = StatModifier.wisdom;

            // Retain the amount in the old array
            for (int i = 0; i < StatModifierLength; i++)
            {
                characterStats[i].amount = characterStat[i].amount;
            }

            // Restore the old array
            characterStat = characterStats;
        }
    }
}
