using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Dice Reward UI Block Classifier, also hold the reference to its child
/// </summary>
public class DiceRewardUIBlock : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Dice Location of this UI Block")]
    public RectTransform diceLocation;
    [Tooltip("Dice Reward Number Text of this UI Block")]
    public TMP_Text diceRewardNumberText;
}
