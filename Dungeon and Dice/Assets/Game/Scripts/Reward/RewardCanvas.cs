using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Reward Canvas identifier, also responsible for the behaviour everything in the reward canvas
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class RewardCanvas : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Transform rewardDisplayContent;

    [Tooltip("Dice Reward Ui Block prefeb")]
    [SerializeField] DiceRewardUIBlock diceRewardUIBlockPrefeb;

    [Header("Events")]
    [Tooltip("Raise when the next level button is clicked")]
    public UnityEvent FinishBattleEncounter;

    public void FinishEncounter()
    {
        // Hide the reward canvas
        HideCanvas();

        // Hide battle encounter interface
        FinishBattleEncounter.Invoke();
    }

    // Called when a reward dice block is requested to be shown
    public void InstantiateRewardDiceBlock(DungeonInventory.DiceNumber diceNumber)
    {
        // Show the reward canvas
        ShowCanvas();

        // Instantiate the reward UI block
        DiceRewardUIBlock diceRewardUIBlock = Instantiate(diceRewardUIBlockPrefeb, rewardDisplayContent);

        // Find the matching dice size
        for (int i = 0; i < DiceManager.Instance.dieMatching.Length; i++)
        {
            // If I find the right dice size
            if (diceNumber.dieSize == DiceManager.Instance.dieMatching[i].dieSize)
            {
                // Instantiate the dice prefeb there
                Dice die = Instantiate(DiceManager.Instance.dieMatching[i].diePrefeb, diceRewardUIBlock.diceLocation).GetComponent<Dice>();

                // Resize the dice to match the UI
                die.SetDiceSize(diceRewardUIBlock.diceLocation.sizeDelta);

                try
                {
                    // Disable the dragging
                    die.GetComponent<DiceDragHandler>().enabled = false;
                }
                catch
                {
                    // Throw a debug message
                    Debug.LogError("There is no DiceDragHandler component on the die!");
                }

                // Set the reward number text to match the dice reward
                diceRewardUIBlock.diceRewardNumberText.text = diceNumber.amount.ToString();

                // Stop the search
                break;
            }
        }
    }

    // Method to show the reward canvas
    public void ShowCanvas()
    {
        // Show the reward canvas
        GetComponent<CanvasGroup>().alpha = 1;
    }

    // Method to hide the reward canvas and clean up the rewardDisplayContent
    public void HideCanvas()
    {
        // Hide the reward canvas
        GetComponent<CanvasGroup>().alpha = 0;

        // Delete what was in the rewardDisplayContent
        for (int i = 0; i < rewardDisplayContent.childCount; i++)
        {
            Destroy(rewardDisplayContent.GetChild(i).gameObject);
        }
    }
}
