using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// A script that is responsible for path button's behaviour
/// </summary>
[RequireComponent(typeof(Button))]
public class PathButton : MonoBehaviour
{
    [Header("Data")]
    [Tooltip("The encounter that is button is represending")]
    public Encounter encounter = null;

    [Header("Events")]
    [Tooltip("Raise when the button is clicked")]
    public UnityEvent<Encounter> selectedPath;

    // Function called on button click, it broadcasts an event where the Path Canvas and Game Manager listen to
    public void SelectedPath()
    {
        // Throw a debug message
        Debug.Log($"Selected {encounter.name}");

        // Invoke the selectedPath event
        selectedPath.Invoke(encounter);
    }
}
