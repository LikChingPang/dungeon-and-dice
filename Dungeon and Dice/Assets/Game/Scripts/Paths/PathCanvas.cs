using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class holds the references of the path canvas and responsible for responding to events from the game (mainly game manager)
/// </summary>
[RequireComponent(typeof(Canvas), typeof(CanvasGroup))]
public class PathCanvas : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Canvas Group component of this path canvas")]
    CanvasGroup myCanvasGroup;
    [Tooltip("The path buttons' parent transform")]
    [SerializeField] Transform paths;
    [Tooltip("The path button's prefeb game object")]
    [SerializeField] PathButton pathButtonPrefeb;

    void Start()
    {
        // Intialize
        myCanvasGroup = GetComponent<CanvasGroup>();

        // Hide the canvas
        HideCanvas();
    }

    // Method to setup path buttons in paths parent (Callback)
    public void SetupPathButtons(Level level)
    {
        // if the paths parent currently does not have enough path buttons to support the level
        if (paths.childCount < level.paths.Length)
        {
            // Calculate the difference
            int difference = level.paths.Length - paths.childCount;

            // Instantiate the missing path buttons
            for (int i = 0; i < difference; i++)
            {
                Instantiate(pathButtonPrefeb, paths);
            }
        }

        for (int i = 0; i < level.paths.Length; i++)
        {
            try
            {
                // Show the path buttons
                paths.GetChild(i).gameObject.SetActive(true);
            }
            catch
            {
                // Throw a debug message
                Debug.LogError($"Child is missing for {paths.name}!");
            }

            try
            {
                // Assign the encounter to the button
                paths.GetChild(i).GetComponent<PathButton>().encounter = level.paths[i];
            }
            catch
            {
                // Throw a debug message
                Debug.LogError($"{paths.GetChild(i).name} does not have PathButton coomponent!");
            }
        }

        // Show the canvas
        ShowCanvas();
    }

    // Mathod to hide all path buttons (callback)
    public void SelectedPath()
    {
        // Hide all path buttons
        for (int i = 0; i < paths.childCount; i++)
        {
            paths.GetChild(i).gameObject.SetActive(false);
        }

        // Hide the canvas
        HideCanvas();
    }

    // Method to show the canvas
    private void ShowCanvas()
    {
        // Show the canvas
        myCanvasGroup.alpha = 1;

        // Allow interacting with this canvas
        myCanvasGroup.blocksRaycasts = true;
        myCanvasGroup.interactable = true;
    }

    // Method to hide the canvas
    private void HideCanvas()
    {
        // Hide the canvas
        myCanvasGroup.alpha = 0;

        // Disable interaction with this canvas
        myCanvasGroup.blocksRaycasts = false;
        myCanvasGroup.interactable = false;
    }
}
