using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Manager to handle all comunications from game world to UI for UI display
/// </summary>
public class UIManager : Singleton<UIManager>
{
    [Header("Reference")]
    [Tooltip("Target marker prefeb that will be instantiated")]
    [SerializeField] RectTransform targetMarker;
    [Tooltip("Skill button for character that will be instantiated")]
    [SerializeField] RectTransform skillButtonPrefeb;
    [Tooltip("Upgrade dropbox for character that will be instantiated")]
    [SerializeField] RectTransform upgradeDropboxPrefeb;
    [Tooltip("Damage Popup Text prefeb that will be instantiated")]
    [SerializeField] RectTransform damagePopupText;
    [Tooltip("Color of health damage Popup Text prefeb that will be instantiated")]
    [SerializeField] Color damagePopupTextColor;
    [Tooltip("Color of shield damage Popup Text prefeb that will be instantiated")]
    [SerializeField] Color shieldPopupTextColor;
    [Tooltip("Color of healing Popup Text prefeb that will be instantiated")]
    [SerializeField] Color healingPopupTextColor;
    [Tooltip("Color of shielding Popup Text prefeb that will be instantiated")]
    [SerializeField] Color shieldingPopupTextColor;
    [Tooltip("Canvas that the gameplay related object should be in")]
    public Canvas gameplayCanvas;
    [Tooltip("Canvas that the hud related object should be in")]
    public Canvas HudCanvas;
    [Tooltip("Skill buttons' parent game object's transform")]
    [SerializeField] Transform skillButtonsParentTransform;
    [Tooltip("Upgrade buttons' parent game object's transform")]
    [SerializeField] Transform upgradeDropboxParentTransform;
    [Tooltip("Reference to the instantiated target marker")]
    RectTransform instantiatedTargetMarker = null;

    [Header("Events")]
    [Tooltip("Raise when skill buttons need to show up")]
    public UnityEvent ShowAllSkillButtons;

    // Method to instantiated a target marker / move existing target marker
    public void CreateTargetMarker(Transform hitTransform)
    {
        // If there is no target marker instantiated into the game yet
        if (instantiatedTargetMarker == null)
        {
            // Instatiate a target marker gameObject in the Canvas
            instantiatedTargetMarker = Instantiate(targetMarker, HudCanvas.transform).GetComponent<RectTransform>();
        }

        // Scale up the targeting marker while keeping the target marker instantiated a square
        // If the local scale x of hitTransform is larger than that of y
        // Make 100% that the target marker is a square
        if (hitTransform.localScale.x >= hitTransform.localScale.y)
        {
            instantiatedTargetMarker.sizeDelta = new Vector2(targetMarker.sizeDelta.x * hitTransform.localScale.x, targetMarker.sizeDelta.x * hitTransform.localScale.x);
        }
        else
        {
            instantiatedTargetMarker.sizeDelta = new Vector2(targetMarker.sizeDelta.y * hitTransform.localScale.y, targetMarker.sizeDelta.y * hitTransform.localScale.y);
        }

        // Change the instantiated target marker's centre
        // Here assumes all transform are using pivot at the bottom center
        instantiatedTargetMarker.position = Camera.main.WorldToScreenPoint(hitTransform.position + new Vector3(0, hitTransform.localScale.y * 0.25f, 0));
    }

    // Method to destory the existing instantiated target marker
    public void RemoveTargetMarker()
    {
        try
        {
            // Destory the instantiated target marker's game object
            Destroy(instantiatedTargetMarker.gameObject);
        }
        catch
        {
            // Throw a debug message
            Debug.LogError("instantiatedTargetMarker is already destroyed!");
        }

        // Remove the referernce to the previous instantiated target marker
        instantiatedTargetMarker = null;
    }

    // Called when CharacterTakeHealthDamage is raised
    public void PopupHealthDamageText(Transform popupPoint, int amount)
    {
        try
        {
            // Instantiate a popup damage text object
            RectTransform popupText = InstantiatePopupDamageText(popupPoint, amount);

            // Change the color of the popup text object to match the health damage
            popupText.GetComponent<TMP_Text>().color = damagePopupTextColor;
        }
        catch
        {
            // Throw a debug message
            Debug.LogError("popupPoint is missing!");
        }
    }

    // Called when CharacterTakeShieldDamage is raised
    public void PopupShieldDamageText(Transform popupPoint, int amount)
    {
        try
        {
            // Instantiate a popup damage text object
            RectTransform popupText = InstantiatePopupDamageText(popupPoint, amount);

            // Change the color of the popup text object to match the shield damage
            popupText.GetComponent<TMP_Text>().color = shieldPopupTextColor;
        }
        catch
        {
            // Throw a debug message
            Debug.LogError("popupPoint is missing!");
        }
    }

    // Called when CharacterTakeHealing is raised
    public void PopupHealingText(Transform popupPoint, int amount)
    {
        try
        {
            // Instantiate a popup damage text object
            RectTransform popupText = InstantiatePopupDamageText(popupPoint, amount);

            // Change the color of the popup text object to match the health healing
            popupText.GetComponent<TMP_Text>().color = healingPopupTextColor;
        }
        catch
        {
            // Throw a debug message
            Debug.LogError("popupPoint is missing!");
        }
    }

    // Called when CharacterTakeShielding is raised
    public void PopupShieldingText(Transform popupPoint, int amount)
    {
        try
        {
            // Instantiate a popup damage text object
            RectTransform popupText = InstantiatePopupDamageText(popupPoint, amount);

            // Change the color of the popup text object to match the shield healing
            popupText.GetComponent<TMP_Text>().color = shieldingPopupTextColor;
        }
        catch
        {
            // Throw a debug message
            Debug.LogError("popupPoint is missing!");
        }
    }

    // Method to instantiate a popup damage text object
    private RectTransform InstantiatePopupDamageText(Transform popupPoint, int amount)
    {
        // Instantiate the damage popup text at location specified
        RectTransform popupText = Instantiate(damagePopupText, popupPoint);

        // Snap it to the point
        popupText.localPosition = Vector2.zero;

        try
        {
            // Change the text of the popup text to match the amount of damage
            popupText.GetComponent<TMP_Text>().text = amount.ToString();
        }
        catch
        {
            // Throw a debug message
            Debug.Log("The damage popup text prefeb does not have a TMP_Text component!");
        }

        return popupText;
    }

    // Method to instantiate and setup skill buttons for player
    public void SetupSkillButtons(SkillData[] skillDatas)
    {
        // If there are not enough skill buttons
        if (skillButtonsParentTransform.childCount < skillDatas.Length)
        {
            // Get the difference between the skill datas and the child count of the parent
            int difference = skillDatas.Length - skillButtonsParentTransform.childCount;

            // Instantiate them
            for (int i = 0; i < difference; i++)
            {
                try
                {
                    Skill skill = Instantiate(skillButtonPrefeb, skillButtonsParentTransform).GetComponent<Skill>();

                    // Set the skill data
                    skill.skillData = skillDatas[i];

                    // Set up the skill UI
                    skill.SetupSkill();
                }
                catch
                {
                    Debug.LogError($"The prefeb {skillButtonPrefeb.name} does not have a Skill component!");
                }
            }
        }
        // Else if there are too many skill buttons
        // Bug may raise here because of Destroy calls
        else if (skillButtonsParentTransform.childCount > skillDatas.Length)
        {
            // Get the difference between the skill datas and the child count of the parent
            int difference = skillButtonsParentTransform.childCount - skillDatas.Length;

            // Get the child count of the parent
            int childCount = skillButtonsParentTransform.childCount;

            // Destroy them
            for (int i = 0; i < difference; i++)
            {
                Destroy(skillButtonsParentTransform.GetChild(childCount - i));
            }

            // Set the skills up
            for (int i = 0; i < skillButtonsParentTransform.childCount; i++)
            {
                try
                {
                    // Cache the skill
                    Skill skill = skillButtonsParentTransform.GetChild(i).GetComponent<Skill>();

                    // Set the skill data
                    skill.skillData = skillDatas[i];

                    // Set up the skill UI
                    skill.SetupSkill();
                }
                catch
                {
                    Debug.LogError($"The prefeb {skillButtonsParentTransform.GetChild(i).name} does not have a Skill component!");
                }
            }
        }
        // If they have the same amount
        else
        {
            // Set the skills up
            for (int i = 0; i < skillButtonsParentTransform.childCount; i++)
            {
                try
                {
                    // Cache the skill
                    Skill skill = skillButtonsParentTransform.GetChild(i).GetComponent<Skill>();

                    // Set the skill data
                    skill.skillData = skillDatas[i];

                    // Set up the skill UI
                    skill.SetupSkill();
                }
                catch
                {
                    Debug.LogError($"The prefeb {skillButtonsParentTransform.GetChild(i).name} does not have a Skill component!");
                }
            }
        }

        // Show the skill button UI
        ShowAllSkillButtons.Invoke();
    }

    // Method to instantiate and setup upgrade dropboxes for player (Callback when Setup Upgrade Dropboxes event is raised)
    public void SetupUpgradeDropboxes(SkillData[] skillDatas)
    {
        // If there are not enough upgrade dropboxes
        if (upgradeDropboxParentTransform.childCount < skillDatas.Length)
        {
            // Get the difference between the skill datas and the child count of the parent
            int difference = skillDatas.Length - upgradeDropboxParentTransform.childCount;

            // Instantiate them
            for (int i = 0; i < difference; i++)
            {
                try
                {
                    Skill skill = Instantiate(upgradeDropboxPrefeb, upgradeDropboxParentTransform).GetComponent<Skill>();

                    // Set the skill data
                    skill.skillData = skillDatas[i];

                    // Set up the skill UI
                    skill.SetupSkill();
                }
                catch
                {
                    Debug.LogError($"The prefeb {upgradeDropboxParentTransform.name} does not have a Skill component!");
                }
            }
        }
        // Else if there are too many upgrade dropboxds
        // Bug may raise here because of Destroy calls
        else if (upgradeDropboxParentTransform.childCount > skillDatas.Length)
        {
            // Get the difference between the skill datas and the child count of the parent
            int difference = upgradeDropboxParentTransform.childCount - skillDatas.Length;

            // Get the child count of the parent
            int childCount = upgradeDropboxParentTransform.childCount;

            // Destroy them
            for (int i = 0; i < difference; i++)
            {
                Destroy(upgradeDropboxParentTransform.GetChild(childCount - i));
            }

            // Set the skills up
            for (int i = 0; i < upgradeDropboxParentTransform.childCount; i++)
            {
                try
                {
                    // Cache the skill
                    Skill skill = upgradeDropboxParentTransform.GetChild(i).GetComponent<Skill>();

                    // Set the skill data
                    skill.skillData = skillDatas[i];

                    // Set up the skill UI
                    skill.SetupSkill();
                }
                catch
                {
                    Debug.LogError($"The prefeb {upgradeDropboxParentTransform.GetChild(i).name} does not have a Skill component!");
                }
            }
        }
        // If they have the same amount
        else
        {
            // Set the skills up
            for (int i = 0; i < upgradeDropboxParentTransform.childCount; i++)
            {
                try
                {
                    // Cache the skill
                    Skill skill = upgradeDropboxParentTransform.GetChild(i).GetComponent<Skill>();

                    // Set the skill data
                    skill.skillData = skillDatas[i];

                    // Set up the skill UI
                    skill.SetupSkill();
                }
                catch
                {
                    Debug.LogError($"The prefeb {upgradeDropboxParentTransform.GetChild(i).name} does not have a Skill component!");
                }
            }
        }
    }
}
