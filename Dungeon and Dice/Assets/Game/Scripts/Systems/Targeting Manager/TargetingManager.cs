using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Manager to handle all comunications between targeting system and Game Manager
/// </summary>
public class TargetingManager : Singleton<TargetingManager>
{
    [Header("Data")]
    [Tooltip("Boolean to determine if the user can change target")]
    [ReadOnly] public bool canTarget = true;

    [Header("Events")]
    [Tooltip("Raise when player targets a character")]
    public UnityEvent<Character> ChangeTarget;

    // Method to ask the UI manager to change the UI display for target marker
    public void InvokeChangeTarget(Transform transform)
    {
        try
        {
            ChangeTarget.Invoke(transform.GetComponent<Character>());
        }
        catch
        {
            // Throw a debug message
            Debug.Log($"{transform.name} does not have a Character component!");
        }
    }

    // Method to enable changing target (Callback when End Turn event is raised)
    public void EnableTargeting()
    {
        canTarget = true;
    }

    // Method to disable changing target (Callback when Skill Seclected event is raised)
    public void DisableTargeting()
    {
        canTarget = false;
    }
}
