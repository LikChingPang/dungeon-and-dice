using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Game Manager should be the one who communicate between game world and systems
/// This game manager does not support the lobby menu
/// </summary>
public class GameManager : Singleton<GameManager>
{
    [Header("References")]
    [Tooltip("An empty game object for the enemies instantiated to help organise the hierarchy")]
    [SerializeField] Transform enemiesParent;

    [Header("Data")]
    [Tooltip("Type effectiveness reference table for the game")]
    public SkillTypeEffectivenessTable effectivenessTable;
    [Tooltip("How long should a turn flip to the other character? (Must be larger than 0)")]
    [SerializeField] float turnFilpTimeRequired = 1.0f;
    [Tooltip("Flag to see if the game manager is currently passing turn to other characters")]
    bool passingTurn = false;
    [Tooltip("What is the percentage of the user getting a chance of critical hit?")]
    public float percentageOfCritical = 0.0f;
    [Tooltip("Critical bonus damage / healing multiplier")]
    public float criticalMultiplier = 0.0f;
    [Tooltip("Player's inventory")]
    public DungeonInventory dungeonInventory;
    [Tooltip("Map scriptable object of the dungeon")]
    public Map map;
    [Tooltip("Current level of dungeon")]
    [SerializeField] [ReadOnly] int currentLevel = 0;
    [Tooltip("Levels of dungeon in this map")]
    [ReadOnly] public Level[] levels;
    [Tooltip("List of active players, there should only be 1 player most of the time")]
    [ReadOnly] public List<Player> playerList;
    [Tooltip("List of active enemies")]
    [ReadOnly] public List<Enemy> enemyList;
    [Tooltip("Which character is taking their turn")]
    [ReadOnly] public Character currentTurnCharacter = null;
    [Tooltip("Which player is taking their turn")]
    [ReadOnly] public Player currentPlayer = null;
    [Tooltip("Which player should take their turn next in the form of index")]
    [ReadOnly] public int nextPlayerTurnIndex = 0;
    [Tooltip("Which enemy should take their turn next in the form of index")]
    [ReadOnly] public int nextEnemyTurnIndex = 0;
    [Tooltip("Last player that the user targets")]
    [ReadOnly] public Player currentPlayerTarget = null;
    [Tooltip("Last enemy that the user targets")]
    [ReadOnly] public Enemy currentEnemyTarget = null;
    [Tooltip("The current encounter")]
    [ReadOnly] public Encounter currentEncounter;
    [Tooltip("Current encounter's win condition")]
    [ReadOnly] public List<Enemy> currentWinCondition;

    [Header("Events")]
    //[Tooltip("Raise when the user can change target")]
    //public UnityEvent EnableTargeting;
    [Tooltip("Raise when the player needs to choose a path")]
    public UnityEvent<Level> SetupPathButtons;
    [Tooltip("Raise when it is needed to remove a target marker")]
    public UnityEvent RemoveTargetMarker;
    [Tooltip("Raise when it is the start of a battle encounter")]
    public UnityEvent StartBattleEncounter;
    [Tooltip("Raise when it is the start of a rest encounter")]
    public UnityEvent StartRestEncounter;
    [Tooltip("Raise when a turn should pass to someone")]
    public UnityEvent<Character> CharacterShouldTakeTurn;
    [Tooltip("Raise when a number of dice is needed to add to the dungeon inventory")]
    public UnityEvent<DungeonInventory.DiceNumber> AddDiceToInventory;

    // temp
    void Start()
    {
        // Start the game
        InitializeDungeon();
    }
    // temp

    void OnValidate()
    {
        // If the designer make the percentage larger than 100%
        if (percentageOfCritical > 100)
        {
            // Make it 100% critical hit
            percentageOfCritical = 100.0f;
        }
    }

    // Method to add the player to the player list
    public void AddPlayer(Player player)
    {
        // Throw a debug message
        Debug.Log($"Adding {player.name} into the player list");

        // Add the player to the player list
        playerList.Add(player);
    }

    // Method to add the enemy to the enemy list
    public void AddEnemy(Enemy enemy)
    {
        // Throw a debug message
        Debug.Log($"Adding {enemy.name} into the enemy list");

        // Add the enemy to the enemy list
        enemyList.Add(enemy);
    }

    // Method to remove the player to the player list
    public void RemovePlayer(Player player)
    {
        // Throw a debug message
        Debug.Log($"Removing {player.name} from the player list");

        // If the player is the current targetted player
        if (player == currentPlayerTarget)
        {
            // Remove the reference to this player for current targetted player
            currentPlayerTarget = null;

            // Ask the UI system to remove the UI display on the dead enemy
            RemoveTargetMarker.Invoke();
        }

        // TODO : Play the death animation in the animator

        //// Remove the player to the player list
        //playerList.Remove(player);
    }

    // Method to remove the enemy to the enemy list
    public void RemoveEnemy(Enemy enemy)
    {
        // Throw a debug message
        Debug.Log($"Removing {enemy.name} from the enemy list");

        //// Remove the enemy to the enemy list
        //enemyList.Remove(enemy);

        // If the enemy is the current targetted enemy
        if (enemy == currentEnemyTarget)
        {
            // Remove the reference to this enemy for current targetted enemy
            currentEnemyTarget = null;

            // Ask the UI system to remove the UI display on the dead enemy
            RemoveTargetMarker.Invoke();
        }
    }

    public void ChangeTarget(Character character)
    {
        // Throw a debug message
        Debug.Log($"Changing target to {character.name}");

        // If the character is a player
        if (character.GetType() == typeof(Player))
        {
            // Set targeted player to that player
            currentPlayerTarget = (Player)character;
        }
        // If the character is an enemy
        else if (character.GetType() == typeof(Enemy))
        {
            // Set targeted enemy to that enemy
            currentEnemyTarget = (Enemy)character;
        }
    }

    // Method to update a character's attack
    public void UpdateCharacterAttack(Character character, int attack)
    {
        // Throw a debug message
        Debug.Log($"{gameObject.name} is updating {character.name}'s attack");
    }

    // Method to update a character's current health
    public void UpdateCharacterCurrentHealth(Character character, int currentHealth)
    {
        // Throw a debug message
        Debug.Log($"{gameObject.name} is updating {character.name}'s current health");
    }

    // Method to update a character's maximum health
    public void UpdateCharacterMaximumHealth(Character character, int maximumHealth)
    {
        // Throw a debug message
        Debug.Log($"{gameObject.name} is updating {character.name}'s maximum health");
    }

    // Method to update a character's shield
    public void UpdateCharacterShield(Character character, int shield)
    {
        // Throw a debug message
        Debug.Log($"{gameObject.name} is updating {character.name}'s shield");
    }

    // Method to start an encounter
    public void StartEncounter(Encounter encounter)
    {
        // Throw a debug message
        Debug.Log($"Starting encounter: {encounter.name}");

        // Set the current encounter to the encounter
        currentEncounter = encounter;

        // If the encounter is a battle
        if (encounter.GetType() == typeof(Battle))
        {
            // Start enable battle interface
            StartBattleEncounter.Invoke();

            // Cast the encounter as Battle
            Battle battle = (Battle)encounter;

            // Instantiate and store the enemies into the game
            for (int i = 0; i < battle.enemies.Length; i++)
            {
                Instantiate(battle.enemies[i], enemiesParent);
            }

            // Record the win condition of the encounter
            for (int i = 0; i < battle.winCondition.Length; i++)
            {
                currentWinCondition.Add(enemyList[battle.winCondition[i]]);
            }

            // Maybe queue up the turn order depending on speed of characters (not current design)
            // Initialize turn order
            nextPlayerTurnIndex = 0;
            nextEnemyTurnIndex = 0;

            // Let the first player character act first
            currentTurnCharacter = playerList[nextPlayerTurnIndex];
            currentPlayer = playerList[nextPlayerTurnIndex++];

            // Ask the current player character to act
            CharacterShouldTakeTurn.Invoke(currentTurnCharacter);
        }
        else if (encounter.GetType() == typeof(Resting))
        {
            // Start enable rest interface
            StartRestEncounter.Invoke();

            // Cast the encounter as Resting
            Resting resting = (Resting)encounter;
        }
    }

    // Method to finish an encounter
    public void FinishEncounter()
    {
        // Throw a debug message
        Debug.Log($"Finishing encounter: {currentEncounter.name}");

        // If the encounter is a battle
        if (currentEncounter.GetType() == typeof(Battle))
        {
            // Cast the current encounter as Battle
            Battle currentBattle = (Battle)currentEncounter;

            // Throw a debug message
            Debug.Log("Giving dice rewards to the player...");

            // Cache the dice reward
            DungeonInventory.DiceNumber[] encounterReward = currentBattle.reward.diceReward;

            // Give out the dice reward to the player
            for (int i = 0; i < encounterReward.Length; i++)
            {
                AddDiceToInventory.Invoke(encounterReward[i]);
            }

            // Remove all enemies out of the game
            for (int i = 0; i < enemiesParent.childCount; i++)
            {
                // Destory the enemy game object
                Destroy(enemiesParent.GetChild(i).gameObject);
            }
        }

        // Clear the current targets
        currentPlayerTarget = null;
        currentEnemyTarget = null;

        // Remove current encounter
        currentEncounter = null;

        // Clear the enemy list just to be safe
        enemyList.Clear();

        // Clear current win condition list just to be safe
        currentWinCondition.Clear();

        // Ask the UI Manager to try and remove the target marker, will fail if the marker is already removed when enemy is dead
        RemoveTargetMarker.Invoke();

        try
        {
            // Ask the dice roll tray to remove all the dice
            DiceRollTray.Instance.RemoveDice();
        }
        catch
        {
            Debug.LogError("DiceRollTray does not exist!");
        }
    }

    // Return "true" if the win condition is met
    private bool CheckFulfillWinCondition(Battle battle)
    {
        // Compare every enemy in the enemy list and check if the win condition enemy is in there
        for (int i = 0; i < battle.winCondition.Length; i++)
        {
            for (int j = 0; j < enemyList.Count; j++)
            {
                // If the win condition enemy is still in the enemy list
                if (currentWinCondition[i] == enemyList[j])
                {
                    // If the enemy is dead
                    if (enemyList[j].currentHealth > 0)
                    {
                        // Throw a debug message
                        Debug.Log("Win condition is not met");

                        // Return false
                        return false;
                    }

                    // Go to the next enemy in the win condition
                    break;
                }
            }
        }
        // Throw a debug message
        Debug.Log("Win condition is met");

        // If the win condition enemies are all absent in the enemy list, return true
        return true;
    }

    // Method to end a turn and swap to the opposite side
    // Beware of infinite loop here (recurion alert)
    public void EndTurn()
    {
        // Cast the current encounter as a battle
        Battle battle = (Battle)currentEncounter;

        // If the win condition is met
        if (CheckFulfillWinCondition(battle) == true)
        {
            // Finish the encounter
            FinishEncounter();

            // Stop the battle
            return;
        }

        // If the game manager is not passing turn currently
        if (passingTurn == false)
        {
            // Stop other pass turn request
            passingTurn = true;

            // Pass turn to other characters
            StartCoroutine(PassingTurn());
        }
    }

    private IEnumerator PassingTurn()
    {
        yield return new WaitForSeconds(turnFilpTimeRequired);

        // Throw a debug message
        Debug.Log("Passing turn...");

        // If the player took their turn, swap it to the enemy's side
        if (currentTurnCharacter.GetType() == typeof(Player))
        {
            // Initialize a temp Character array for the function
            Character[] characters = enemyList.ToArray();

            // Pass the turn to the next eligible enemy
            nextEnemyTurnIndex = PassTurn(nextEnemyTurnIndex, characters);

            // Reset current player to prevent weird bugs, enable later
            currentPlayer = null;
        }
        else if (currentTurnCharacter.GetType() == typeof(Enemy))
        {
            // Initialize a temp Character array for the function
            Character[] characters = playerList.ToArray();

            // Pass the turn to the next eligible player
            nextPlayerTurnIndex = PassTurn(nextPlayerTurnIndex, characters);

            // Set the new current player, -1 because we need the current player turn index instead
            currentPlayer = playerList[nextPlayerTurnIndex - 1];
        }

        // Able to pass turn to others again
        passingTurn = false;

        CharacterShouldTakeTurn.Invoke(currentTurnCharacter);
    }

    // Function to pass a turn
    private int PassTurn(int index, Character[] characters)
    {
        // If there is at least a player
        if (characters.Length > 0)
        {
            // If the current index is still within the size of the player list
            if (index < characters.Length)
            {
                // If the next player in the line is still alive
                if (characters[index].currentHealth > 0)
                {
                    // The next player will take their turn
                    currentTurnCharacter = characters[index++];
                }
                // If the next player in the line is dead
                else
                {
                    // Check for the next player in the line
                    index++;
                    index = PassTurn(index, characters);
                }
            }
            // If the index reached the end of the list size
            else
            {
                // Reset the index
                index = 0;

                // If the first player in the list is still alive
                if (characters[index].currentHealth > 0)
                {
                    // The first player in the list will take their turn
                    currentTurnCharacter = characters[index++];
                }
                // If the first player in the list is dead
                else
                {
                    // Check for the next player in the line
                    index++;
                    index = PassTurn(index, characters);
                }
            }
        }

        return index;
    }

    public void InitializeDungeon()
    {
        // Initialize the levels array
        levels = new Level[map.numberOfLevels];

        for (int i = 0; i < map.numberOfLevels; i++)
        {
            levels[i] = new Level();
        }

        // Draw encounters for every level except the final level
        for (int i = 0; i < levels.Length - 1; i++)
        {
            // Initialize that level
            levels[i].Initialize(map.minimumPaths, map.maximumPaths);

            // Pick encounters for each path in each level
            for (int j = 0; j < levels[i].paths.Length; j++)
            {
                // Randomize the pool
                int poolIndex = Random.Range(0, map.encounterPools.Length);

                // Pick the encounter from the randomized pool
                int encoutnerIndex = Random.Range(0, map.encounterPools[poolIndex].encounters.Length);
                levels[i].paths[j] = map.encounterPools[poolIndex].encounters[encoutnerIndex];
            }
        }

        // Initialize the final level level
        levels[levels.Length - 1].Initialize(1, 1);

        // Set it as the boss encounter
        levels[levels.Length - 1].paths[0] = map.bossEncounter;

        // Ask the player to chose the first level
        SetupPathButtons.Invoke(levels[currentLevel]);
    }

    // Method called when the player finished a level (callback for FinishBattleEncounter and FinishHealingEncounter)
    public void ToNextLevel()
    {
        // If there are still more levels to go
        if (++currentLevel < levels.Length)
        {
            // Ask the player to chose the first level
            SetupPathButtons.Invoke(levels[currentLevel]);
        }
        else
        {
            // Throw a debug message
            Debug.Log("You Win!");
        }
    }
}
