using Project.Build.Commands;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inventory storing all dice player collect when in a dungeon
/// Designers are not allows to change the data structure of the inventory, they must first ask the developer to make the change
/// </summary>
public class DungeonInventory : MonoBehaviour
{
    [SerializeField] [ReadOnly] DiceNumber[] myInventory = new DiceNumber[6];

    [System.Serializable]
    public struct DiceNumber
    {
        public DiceManager.DieSize dieSize;
        public int amount;
    }

    void OnValidate()
    {
        // If the designer decide to change the length of the array
        if (myInventory.Length != 6)
        {
            // Initialize a temp Inventory array to store the data
            DiceNumber[] tempInventory = new DiceNumber[6];

            // Copy the data over to the temp array
            for (int i = 0; i < tempInventory.Length; i++)
            {
                tempInventory[i].dieSize = myInventory[i].dieSize;
                tempInventory[i].amount = tempInventory[i].amount;
            }

            // Use the temp array as the new array
            myInventory = tempInventory;
        }
    }

    void Start()
    {
        // Initialize
        for (int i = 0; i < myInventory.Length; i++)
        {
            myInventory[i].dieSize = (DiceManager.DieSize)i;
            myInventory[i].amount = 0;
        }
    }

    public void AddDiceToInventory(DiceNumber diceNumber)
    {
        for (int i = 0; i < myInventory.Length; i++)
        {
            if (myInventory[i].dieSize == diceNumber.dieSize)
            {
                myInventory[i].amount += diceNumber.amount;

                break;
            }
        }
    }
}
