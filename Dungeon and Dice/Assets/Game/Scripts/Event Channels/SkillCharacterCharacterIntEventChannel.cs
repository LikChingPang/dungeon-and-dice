using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillCharacterCharacterIntEventChannel", menuName = "Event/Skill Character Character Int Event Channel")]
public class SkillCharacterCharacterIntEventChannel : ScriptableObject
{
    private List<SkillCharacterCharacterIntEventListener> listeners = new List<SkillCharacterCharacterIntEventListener>();

    public void Raise(Skill skill, Character character1, Character character2, int integer)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(skill, character1, character2, integer);
        }
    }

    public void RegisterListener(SkillCharacterCharacterIntEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(SkillCharacterCharacterIntEventListener listener)
    {
        listeners.Remove(listener);
    }
}
