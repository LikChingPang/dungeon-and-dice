using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillDataSkillDataEventChannel", menuName = "Event/SkillData SkillData Event Channel")]
public class SkillDataSkillDataEventChannel : ScriptableObject
{
    private List<SkillDataSkillDataEventListener> listeners = new List<SkillDataSkillDataEventListener>();

    public void Raise(SkillData skillData1, SkillData skillData2)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(skillData1, skillData2);
        }
    }

    public void RegisterListener(SkillDataSkillDataEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(SkillDataSkillDataEventListener listener)
    {
        listeners.Remove(listener);
    }
}
