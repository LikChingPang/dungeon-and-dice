using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelEventListener : MonoBehaviour
{
    [Tooltip("The event channel scritpable object")]
    public LevelEventChannel channel;
    [Tooltip("Callback to respond to the unity event")]
    public UnityEvent<Level> response;

    private void OnEnable()
    {
        channel.RegisterListener(this);
    }

    private void OnDisable()
    {
        channel.UnregisterListener(this);
    }

    public void OnEventRaised(Level level)
    {
        response.Invoke(level);
    }
}
