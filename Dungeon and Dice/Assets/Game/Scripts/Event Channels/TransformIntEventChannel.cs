using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TransformIntEventChannel", menuName = "Event/Transform Int Event Channel")]
public class TransformIntEventChannel : ScriptableObject
{
    private List<TransformIntEventListener> listeners = new List<TransformIntEventListener>();

    public void Raise(Transform transform, int integer)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(transform, integer);
        }
    }

    public void RegisterListener(TransformIntEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(TransformIntEventListener listener)
    {
        listeners.Remove(listener);
    }
}
