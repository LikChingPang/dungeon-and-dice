using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillDataArrayEventListener : MonoBehaviour
{
    [Tooltip("The event channel scritpable object")]
    public SkillDataArrayEventChannel channel;
    [Tooltip("Callback to respond to the unity event")]
    public UnityEvent<SkillData[]> response;

    private void OnEnable()
    {
        channel.RegisterListener(this);
    }

    private void OnDisable()
    {
        channel.UnregisterListener(this);
    }

    public void OnEventRaised(SkillData[] skillDatas)
    {
        response.Invoke(skillDatas);
    }
}
