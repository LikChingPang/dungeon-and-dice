using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DiceNumberEventListener : MonoBehaviour
{
    [Tooltip("The event channel scritpable object")]
    public DiceNumberEventChannel channel;
    [Tooltip("Callback to respond to the unity event")]
    public UnityEvent<DungeonInventory.DiceNumber> response;

    private void OnEnable()
    {
        channel.RegisterListener(this);
    }

    private void OnDisable()
    {
        channel.UnregisterListener(this);
    }

    public void OnEventRaised(DungeonInventory.DiceNumber diceNumber)
    {
        response.Invoke(diceNumber);
    }
}
