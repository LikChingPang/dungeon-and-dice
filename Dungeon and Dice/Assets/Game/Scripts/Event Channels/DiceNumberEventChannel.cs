using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DiceNumberEventChannel", menuName = "Event/DiceNumber Event Channel")]
public class DiceNumberEventChannel : ScriptableObject
{
    private List<DiceNumberEventListener> listeners = new List<DiceNumberEventListener>();

    public void Raise(DungeonInventory.DiceNumber diceNumber)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(diceNumber);
        }
    }

    public void RegisterListener(DiceNumberEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(DiceNumberEventListener listener)
    {
        listeners.Remove(listener);
    }
}
