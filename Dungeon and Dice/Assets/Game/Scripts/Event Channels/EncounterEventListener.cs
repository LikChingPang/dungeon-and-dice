using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EncounterEventListener : MonoBehaviour
{
    [Tooltip("The event channel scritpable object")]
    public EncounterEventChannel channel;
    [Tooltip("Callback to respond to the unity event")]
    public UnityEvent<Encounter> response;

    private void OnEnable()
    {
        channel.RegisterListener(this);
    }

    private void OnDisable()
    {
        channel.UnregisterListener(this);
    }

    public void OnEventRaised(Encounter encounter)
    {
        response.Invoke(encounter);
    }
}
