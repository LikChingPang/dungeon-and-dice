using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelEventChannel", menuName = "Event/Level Event Channel")]
public class LevelEventChannel : ScriptableObject
{
    private List<LevelEventListener> listeners = new List<LevelEventListener>();

    public void Raise(Level level)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(level);
        }
    }

    public void RegisterListener(LevelEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(LevelEventListener listener)
    {
        listeners.Remove(listener);
    }
}
