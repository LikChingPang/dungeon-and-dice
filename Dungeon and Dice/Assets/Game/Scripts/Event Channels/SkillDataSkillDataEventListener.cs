using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillDataSkillDataEventListener : MonoBehaviour
{
    [Tooltip("The event channel scritpable object")]
    public SkillDataSkillDataEventChannel channel;
    [Tooltip("Callback to respond to the unity event")]
    public UnityEvent<SkillData, SkillData> response;

    private void OnEnable()
    {
        channel.RegisterListener(this);
    }

    private void OnDisable()
    {
        channel.UnregisterListener(this);
    }

    public void OnEventRaised(SkillData skillData1, SkillData skillData2)
    {
        response.Invoke(skillData1, skillData2);
    }
}
