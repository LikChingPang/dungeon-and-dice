using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillCharacterCharacterIntEventListener : MonoBehaviour
{
    [Tooltip("The event channel scritpable object")]
    public SkillCharacterCharacterIntEventChannel channel;
    [Tooltip("Callback to respond to the unity event")]
    public UnityEvent<Skill, Character, Character, int> response;

    private void OnEnable()
    {
        channel.RegisterListener(this);
    }

    private void OnDisable()
    {
        channel.UnregisterListener(this);
    }

    public void OnEventRaised(Skill skill, Character character1, Character character2, int integer)
    {
        response.Invoke(skill, character1, character2, integer);
    }
}
