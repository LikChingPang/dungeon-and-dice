using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TransformIntEventListener : MonoBehaviour
{
    [Tooltip("The event channel scritpable object")]
    public TransformIntEventChannel channel;
    [Tooltip("Callback to respond to the unity event")]
    public UnityEvent<Transform, int> response;

    private void OnEnable()
    {
        channel.RegisterListener(this);
    }

    private void OnDisable()
    {
        channel.UnregisterListener(this);
    }

    public void OnEventRaised(Transform transform, int integer)
    {
        response.Invoke(transform, integer);
    }
}
