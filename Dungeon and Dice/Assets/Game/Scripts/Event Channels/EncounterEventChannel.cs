using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EncounterEventChannel", menuName = "Event/Encounter Event Channel")]
public class EncounterEventChannel : ScriptableObject
{
    private List<EncounterEventListener> listeners = new List<EncounterEventListener>();

    public void Raise(Encounter encounter)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(encounter);
        }
    }

    public void RegisterListener(EncounterEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(EncounterEventListener listener)
    {
        listeners.Remove(listener);
    }
}
