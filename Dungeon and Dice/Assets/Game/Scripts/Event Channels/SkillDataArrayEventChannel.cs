using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillDataArrayEventChannel", menuName = "Event/SkillData[] Event Channel")]
public class SkillDataArrayEventChannel : ScriptableObject
{
    private List<SkillDataArrayEventListener> listeners = new List<SkillDataArrayEventListener>();

    public void Raise(SkillData[] skillDatas)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(skillDatas);
        }
    }

    public void RegisterListener(SkillDataArrayEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(SkillDataArrayEventListener listener)
    {
        listeners.Remove(listener);
    }
}
