using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object that stores an encounter pool for the game to draw from
/// </summary>
[CreateAssetMenu(fileName = "Encounter Pool", menuName = "Encounter/Encounter Pool")]
public class EncounterPool : ScriptableObject
{
    [Header("Data")]
    [Tooltip("An array of encoutners in the pool")]
    public Encounter[] encounters;
    [Tooltip("What type of encounter that this encounter pool is representing for")]
    public EncounterType encounterType;

    public enum EncounterType
    {
        NormalBattle,
        EliteBattle,
        Event,
        Resting,
        BossBattle
    }
}
