using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A scriptable object that holds the data of a map, aka a dungeon
/// </summary>
[CreateAssetMenu(fileName = "Map", menuName = "Map/Map")]
public class Map : ScriptableObject
{
    [Header("Data")]
    [Tooltip("Number of levels in this map")]
    public int numberOfLevels = 1;
    [Tooltip("Minimum number of paths in a level in this map")]
    public int minimumPaths = 1;
    [Tooltip("Maximum number of paths in a level in this map")]
    public int maximumPaths = 1;
    [Tooltip("A List of encounter pools in this map")]
    public EncounterPool[] encounterPools;
    [Tooltip("Boss encounter of this map")]
    public Battle bossEncounter;

    void OnValidate()
    {
        // If the designer input a number less than or equal to 0 for the number of level
        if (numberOfLevels <= 0)
        {
            // Make it positive again
            numberOfLevels = 1;
        }

        // If the designer input a number less than or equal to 0 for the minimum number of paths
        if (minimumPaths <= 0)
        {
            // Make it positive again
            minimumPaths = 1;
        }

        // If the designer input a number less than or equal to 0 for the maximum number of paths
        if (maximumPaths <= 0)
        {
            // Make it positive again
            maximumPaths = 1;
        }
    }
}
