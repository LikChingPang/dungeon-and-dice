using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class that holds the data of a level in the dungeon
/// </summary>
[System.Serializable]
public class Level
{
    [Header("Data")]
    [Tooltip("Paths available for proceeding to next level")]
    public Encounter[] paths;

    public void Initialize(int min, int max)
    {
        int numberOfExits = Random.Range(min, max + 1);

        paths = new Encounter[numberOfExits];
    }
}
