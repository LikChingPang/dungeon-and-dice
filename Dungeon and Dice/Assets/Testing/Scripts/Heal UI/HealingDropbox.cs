using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class stores methods for healing dropbox
/// </summary>
[RequireComponent(typeof(DropHandler))]
public class HealingDropbox : MonoBehaviour
{
    // Method to roll all the current children dice
    public void RollAllDice()
    {
        float rollTime = 0.0f;

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Dice>().RollDie();

            if (transform.GetChild(i).GetComponent<Dice>().rollingTime > rollTime)
            {
                rollTime = transform.GetChild(i).GetComponent<Dice>().rollingTime;
            }
        }

        StartCoroutine(GetResult(rollTime));
    }

    // Method to return all roll results
    public int GetRollResults()
    {
        // Initlaize an int to store the roll results
        int rollResults = 0;

        // Calculate the total amount of roll results
        for (int i = 0; i < transform.childCount; i++)
        {
            rollResults += transform.GetChild(i).GetComponent<Dice>().rollResult;
        }

        //test
        Debug.Log($"Total roll result is {rollResults}");
        //test

        return rollResults;
    }

    // Method to destroy all children gameObjects (dice)
    public void RemoveDice()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    IEnumerator GetResult(float time)
    {
        yield return new WaitForSeconds(time);

        GetRollResults();
    }
}
