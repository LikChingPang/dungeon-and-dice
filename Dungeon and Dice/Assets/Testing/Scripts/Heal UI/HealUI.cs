using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class that holds the reference to its child
/// </summary>
public class HealUI : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Healing Dropbox of this healing UI")]
    public Transform healingDropbox;
    [Tooltip("Dice Holder of this healing UI")]
    public Transform diceHolder;
    [Tooltip("Roll Button of this healing UI")]
    public Button rollButton;
}
