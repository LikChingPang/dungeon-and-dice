using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hidden component for Grid Layout Group 2D
/// </summary>
[RequireComponent(typeof(GridLayoutGroup2D))]
public class GridLayoutGroupController : MonoBehaviour
{
    [Header("Reference")]
    [Tooltip("Grid Layout Group 2D component of this game object")]
    GridLayoutGroup2D gridLayoutGroupEditor;

    void Start()
    {
        // Initialize
        gridLayoutGroupEditor = GetComponent<GridLayoutGroup2D>();

        // Put the game objects in the corresponding place
        PositionObjects();
    }

    // Hide the component in the inspector
    void Reset()
    {
        hideFlags = HideFlags.HideInInspector;
    }

    // Method to position all child game objects in the right coordinate
    private void PositionObjects()
    {
        // Initialize a temp float to keep track of the total distance of the child game objects
        float totalDistance = 0.0f;

        // If the game objects should go along the x-axis
        if (gridLayoutGroupEditor.axis == GridLayoutGroup2D.Axis.x)
        {
            // For every child game object
            for (int i = 0; i < transform.childCount; i++)
            {
                // Reset its position
                transform.GetChild(i).position = Vector2.zero;

                // Calculate the new position for the game object
                transform.GetChild(i).position = new Vector2(transform.position.x + totalDistance + transform.GetChild(i).localScale.x / 2, transform.position.y + transform.GetChild(i).position.y);

                // Calculate how far should the next game object start from the origin
                totalDistance += transform.GetChild(i).localScale.x + gridLayoutGroupEditor.spacing;
            }
        }
        // If the game objects should go along the y-axis
        if (gridLayoutGroupEditor.axis == GridLayoutGroup2D.Axis.y)
        {
            // For every child game object
            for (int i = 0; i < transform.childCount; i++)
            {
                // Reset its position
                transform.GetChild(i).position = Vector2.zero;

                // Calculate the new position for the game object
                transform.GetChild(i).position = new Vector2(transform.position.x + transform.GetChild(i).position.x, transform.position.y + totalDistance + transform.GetChild(i).localScale.y / 2);

                // Calculate how far should the next game object start from the origin
                totalDistance += transform.GetChild(i).localScale.y + gridLayoutGroupEditor.spacing;
            }
        }
    }
}
