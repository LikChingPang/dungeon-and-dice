using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script for holding reference to the rest canvas' children and responsible for rest canvas' event management
/// </summary>
public class RestCanvas : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Upgrade button in the canvas")]
    public Button upgradeButton;
    [Tooltip("Heal button in the canvas")]
    public Button healButton;
    [Tooltip("skill Upgrade Dropboxes' parent game object in the canvas")]
    public Transform skillUpgradeDropboxParent;
    [Tooltip("Heal dropbox for the dice in the canvas")]
    public Transform healingDropbox;

    // Method to display a child UI element
    public void DisplayUI(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
        canvasGroup.interactable = true;
    }

    // Method to hide a child UI element
    public void HideUI(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.interactable = false;
    }
}
