using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Class for upgrade button in the rest encounter
/// </summary>
[RequireComponent(typeof(Button))]
public class UpgradeButton : MonoBehaviour
{
    [Header("Events")]
    [Tooltip("Raise when the upgrade button is clicked")]
    public UnityEvent UpgradeButtonSelected;
    [Tooltip("Raise when the upgrade button needed to be setup")]
    public UnityEvent<SkillData[]> SetupUpgradeDropbox;

    // Method called when this upgrade button is clicked
    public void UpgradeButtonPressed()
    {
        // Ask the UI Manager to setup the upgrade buttons for the current selected player
        SetupUpgradeDropbox.Invoke(GameManager.Instance.currentPlayerTarget.playerStats.selectedSkills);

        // Ask the rest interface to display the upgrade skill UI
        UpgradeButtonSelected.Invoke();
    }
}
