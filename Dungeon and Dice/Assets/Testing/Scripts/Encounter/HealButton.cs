using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// A class for heal button in the rest encounter
/// </summary>
[RequireComponent(typeof(Button))]
public class HealButton : MonoBehaviour
{
    [Header("Events")]
    [Tooltip("Raise when the heal button is clicked")]
    public UnityEvent HealButtonSelected;

    // Method called when this heal button is clicked
    public void HealButtonPressed()
    {
        // Ask the rest interface to display the heal UI
        HealButtonSelected.Invoke();
    }
}
