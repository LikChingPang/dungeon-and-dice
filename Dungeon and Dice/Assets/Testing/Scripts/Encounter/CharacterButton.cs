using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// A class responsible for behaviour of the character buttons in the rest encounter
/// </summary>
[RequireComponent(typeof(Button))]
public class CharacterButton : MonoBehaviour
{
    [SerializeField] [ReadOnly] Player myPlayer;

    public UnityEvent<Character> ChangeTarget;

    // Method to set the player that this character button is responsible for (Callback when add player event is raised)
    public void SetPlayer(Player player)
    {
        myPlayer = player;
    }

    // Method to ask the game manager to change the player target to this character
    public void ChangePlayerTarget()
    {
        ChangeTarget.Invoke(myPlayer);
    }
}
