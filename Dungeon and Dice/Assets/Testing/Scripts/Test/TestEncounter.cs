using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script for testing encounters
/// </summary>
public class TestEncounter : MonoBehaviour
{
    [SerializeField] Player player;

    [SerializeField] Encounter encounter;

    [SerializeField] Battle battle;

    [SerializeField] float encounterTime;

    void Start()
    {
        GameManager.Instance.StartEncounter(encounter);

        //for (int i = 0; i < 4; i++)
        //{
        //    Debug.Log(GameManager.Instance.effectivenessTable.effectiveness[i, 0] + "\n");
        //    Debug.Log(GameManager.Instance.effectivenessTable.effectiveness[i, 1] + "\n");
        //    Debug.Log(GameManager.Instance.effectivenessTable.effectiveness[i, 2] + "\n");
        //    Debug.Log(GameManager.Instance.effectivenessTable.effectiveness[i, 3] + "\n");
        //}
    }
}
