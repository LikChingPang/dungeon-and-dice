using Project.Build.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Script for testing the level class
/// </summary>
public class TestLevel : MonoBehaviour
{
    public Map map;

    [ReadOnly] public Level[] levels;

    public UnityEvent<Level> SetupPathButtons;

    void Start()
    {
        // Initialize the levels array
        levels = new Level[map.numberOfLevels];

        for (int i = 0; i < map.numberOfLevels; i++)
        {
            levels[i] = new Level();
        }

        for (int i = 0; i < levels.Length; i++)
        {
            levels[i].Initialize(map.minimumPaths, map.maximumPaths);

            //levels[i].encounterPool = encounterPools[poolIndex];
            for (int j = 0; j < levels[i].paths.Length; j++)
            {
                // Randomize the pool
                int poolIndex = Random.Range(0, map.encounterPools.Length);

                // Pick the encounter from the randomized pool
                int encoutnerIndex = Random.Range(0, map.encounterPools[poolIndex].encounters.Length);
                levels[i].paths[j] = map.encounterPools[poolIndex].encounters[encoutnerIndex];
            }
        }

        SetupPathButtons.Invoke(levels[0]);
    }
}
